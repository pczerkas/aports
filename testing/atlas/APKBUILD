# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=atlas
pkgver=0.16.0
pkgrel=0
pkgdesc="Database schema migration tool using modern DevOps principles"
url="https://atlasgo.io/"
# x86, armhf, armv7: multiple packages fail to build on 32-bit platforms due to integer overflow
# riscv64: github.com/remyoudompheng/bigfft fails to build
arch="all !x86 !armhf !armv7 !riscv64"
license="Apache-2.0"
makedepends="go sqlite-dev"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/ariga/atlas/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/cmd/atlas"

export CGO_ENABLED=1 # required for sqlite driver
export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o atlas \
		-ldflags "-X ariga.io/atlas/cmd/atlas/internal/cmdapi.version=v$pkgver"

	for shell in bash fish zsh; do
		./atlas completion $shell > atlas.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 atlas -t "$pkgdir"/usr/bin/

	install -Dm644 atlas.bash \
		"$pkgdir"/usr/share/bash-completion/completions/atlas
	install -Dm644 atlas.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/atlas.fish
	install -Dm644 atlas.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_atlas
}

sha512sums="
79d9223e0c0cd7308e8e996d351b6bee0e2e0f960c6ce3ae67a7bd89b95a14725f4937191ec8e6fa9cb2fb85b3301b3adfcdb1edff6b2840fe1f7900d8226a4d  atlas-0.16.0.tar.gz
"
